//
//  CalculatorBrain.swift
//  calculator
//
//  Created by Douglas De Voogt on 9/2/17.
//  Copyright © 2017 Douglas De Voogt. All rights reserved.
//

import Foundation

struct CalculatorBrain {
    private var accumulator: Double?
    
    enum Operand{
        case constant(Double)
        case unaryOp((Double) -> Double)
        case binaryOp((Double,Double) -> Double)
        case equals
    }
    
    private var ops: Dictionary<String,Operand> = [
        "π": Operand.constant(Double.pi),
        "e": Operand.constant(M_E),
        "√": Operand.unaryOp(sqrt),
        "±": Operand.unaryOp({-$0}),
        "×": Operand.binaryOp({$0 * $1}),
        "÷": Operand.binaryOp({$0 / $1}),
        "-": Operand.binaryOp({$0 - $1}),
        "+": Operand.binaryOp({$0 + $1}),
        "=": Operand.equals
    ]
    
    mutating func performOperation(_ symbol: String){
        
        if let operation = ops[symbol] {
            switch operation {
            case .constant(let val):
                accumulator = val
            case .unaryOp(let function):
                if let operand = accumulator {
                    accumulator = function(operand)
                }
            case .binaryOp(let function):
                performPendingBinaryOp()
                if let operand = accumulator {
                    pbo = PendingBinaryOperation(function: function, firstOperand: operand)
                }
            case .equals:
                performPendingBinaryOp()
            }
        }
    }
    
    mutating func setOperand(_ operand: Double) {
        self.accumulator = operand
    }
    
    var result: Double? {
        get {
            return accumulator
        }
    }
    
    private var pbo: PendingBinaryOperation?
    
    private mutating func performPendingBinaryOp(){
        if pbo != nil && accumulator != nil {
            accumulator = pbo!.perform(with: accumulator!)
            pbo = nil
        }
    }
    
    struct PendingBinaryOperation {
        var function: (Double, Double) -> Double
        var firstOperand: Double
        
        func perform(with operand: Double) -> Double {
            return function(firstOperand, operand)
        }
    }
}
