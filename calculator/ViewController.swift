//
//  ViewController.swift
//  calculator
//
//  Created by Douglas De Voogt on 9/2/17.
//  Copyright © 2017 Douglas De Voogt. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var display: UILabel!
    
    private var userIsInMiddleOfTyping = false
    
    @IBAction func displayNumber(_ sender: UIButton) {
        print("\(sender.currentTitle!) pressed")
        var text = sender.currentTitle!
        if userIsInMiddleOfTyping {
            if text == "." && display.text!.contains(".") {
                return
            }
            display.text! += sender.currentTitle!
        } else {
            
            if text == "." {
                text = "0."
            }
            display.text! = text
            userIsInMiddleOfTyping = true
        }
    }
    
    private var displayValue: Double {
        get {
            var val = display.text!
            if val.hasSuffix(".") {
                val += "0"
            }
            return Double(val)!
        }
        set {
            let isInt = floor(newValue) == newValue
            display.text = isInt ? String(Int(newValue)) : String(newValue)
        }
    }
    
    private var calcBrain = CalculatorBrain()
    @IBAction func showMathSymbol(_ sender: UIButton) { //rename performOperation
        if userIsInMiddleOfTyping {
            calcBrain.setOperand(displayValue)
            userIsInMiddleOfTyping = false
        }
        if let mathSymbol = sender.currentTitle {
            calcBrain.performOperation(mathSymbol)
        }
        if let result = calcBrain.result {
            displayValue = result
        }
    }
    
    
}

